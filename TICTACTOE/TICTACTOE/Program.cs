﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace TICTACTOE
{
    public class Program
    {
       
        //I HAVE TO MANY GLOBAL VARIABLE I NEED TO ORGANIZE PROJECT BETTER SO I DONT HAVE AS MUCH
        static string choiceString;
        static string[] board = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
        static int playerCounter = 1;
        static string player1, player2;
        static int endGame;
        static int tieGame;
        static int restart;
        static int numOfTimesPlayed;
        static int moveOn;
        static string playAgain;
        static int gameTypeChoiceCount;
        static string gameTypeChoice;
        static int corner = 0;
        static int playWithComputer = 0;
        static int makeSureSwitchFirstTurn = 2;
        static int gameCounter;
        static int numOfGamesWon1;
        static int numOfGamesWon2;
        static int numOfTiedGame;
        static int oldCount = 1;


        public static void Main(string[] args)
        {
            int choice = 0;
            intro();
            do
            {
                gameCounter++;
                switchPlayers();

                do
                {
                    prepareBoard();
                    checkWhosTurn();

                    do
                    {
                        if (playerCounter % 2 == 0 && playWithComputer == 1)//ONLY TRUE IF PLAYING AGAINST COMPUTER
                        {
                            computerPlay();
                            checkingForWin();
                        }
                        else
                        {   //TAKES USER INPUT
                            choiceString = Console.ReadLine();
                        }

                        //MAKES SURE INPUT IS VALID
                        if (int.TryParse(choiceString, out choice) && choice >= 1 && choice <= 9 && board[choice] != "X" && board[choice] != "O")
                        {
                            checkWhosTurn();
                        }
                        else
                        {  
                            Console.Clear();
                            Console.WriteLine(player1 + " is X and " + player2 + " is O!\n");
                            gameBoard();
                            checkWhosTurn();
                            if (choice < 1 || choice > 9)
                            {
                                Console.WriteLine("\nYou need to pick a number 1-9\n");
                            }

                            else if (board[choice] == "X" || board[choice] == "O")
                            {
                                Console.WriteLine("\nPick a unfilled spot!\n");
                            }
                        }

                    } while (choice < 1 || choice > 9 || board[choice] == "X" || board[choice] == "O");

                    if (board[choice] != "X" && board[choice] != "O")
                    {   //SWITCHS TURNS BETWEEN PLAYERS USING IF ELSE, ALSO CHECKS FOR WIN OR TIE
                        if (playerCounter % 2 == 0)
                        {
                            board[choice] = "O";
                            playerCounter++;
                            if (playWithComputer == 1)
                            {
                                prepareBoard();
                                Console.WriteLine("Computers Move: {0}\n--Review Computers move then press any key to continue--", choice);
                                Console.ReadKey();
                            }
                            if (checkingForWin() == "Tie")
                            {
                                prepareBoard();
                                Console.WriteLine("It was a tie!");
                                Console.WriteLine("Would you like to play again? [Y/N]");
                                tieGame = 1;
                                endGame = 1;
                                numOfTiedGame++;
                            }
                            else if (checkingForWin() == "Win")
                            {
                                prepareBoard();
                                Console.WriteLine("\n " + player2 + " Won!!!");
                                Console.WriteLine("Would you like to play again? [Y/N]");
                                endGame = 1;
                                tieGame = 1;
                                numOfGamesWon2++;
                            }
                        }
                        else
                        {
                            board[choice] = "X";
                            playerCounter++;
                            if (checkingForWin() == "Tie")
                            {
                                prepareBoard();
                                Console.WriteLine("It was a tie!");
                                Console.WriteLine("Would you like to play again? [Y/N]");
                                tieGame = 1;
                                endGame = 1;
                                numOfTiedGame++;
                            }
                            else if (checkingForWin() == "Win")
                            {
                                prepareBoard();
                                Console.WriteLine("\n" + player1 + " Won!!!");
                                Console.WriteLine("Would you like to play again? [Y/N]");
                                endGame = 1;
                                tieGame = 1;
                                numOfGamesWon1++;
                            }
                        }
                    }

                } while (tieGame != 1 || endGame != 1);
                //DECISION IF PLAYER WANTS TO PLAY AGAIN
                do
                {
                    
                    playAgain = Console.ReadLine();
                    if (playAgain == "y" || playAgain == "Y" || playAgain == "n" || playAgain == "N")
                    {
                        createStatsFile();
                        Console.WriteLine("The game info is saved on your desktop in a text file named TicTacToe_Stats.txt");
                        Console.WriteLine("---Press any key to continue---");
                        Console.ReadKey();
                        moveOn = 1;
                    }
                    else
                    {
                        Console.WriteLine(@"You need to enter ""Y"" to play again or ""N"" to quit.");
                        moveOn = 0;
                    }

                } while (moveOn == 0);

                if (playAgain == "y" || playAgain == "Y")
                {   //RESET BOARD AND VALUES IN GLOABL VARIABLES SO USER MAY PLAY AGAIN
                    Array.Clear(board, 0, board.Length);
                    endGame = 0;
                    tieGame = 0;
                    numOfTimesPlayed = 1;
                }
                board = new[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };


                if (playAgain == "n" || playAgain == "N")
                {   //IF PLAYER DOES WANT TO PLAY AGAIN CLOSE PROGRAM
                    restart = 1;
                }

            } while (restart != 1);

        }


        public static void intro()
        {

            Console.WriteLine("Welcome to Tic Tac Toe!\n");
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
            Console.Clear();
            againstComputerOrPerson();
        }
        public static void gameBoard()
        {
            Console.WriteLine("  " + board[1] + " | " + board[2] + " | " + board[3]);
            Console.WriteLine(" -----------");
            Console.WriteLine("  " + board[4] + " | " + board[5] + " | " + board[6]);
            Console.WriteLine(" -----------");
            Console.WriteLine("  " + board[7] + " | " + board[8] + " | " + board[9]);
        }
        public static string checkingForWin()
        {

            if (board[1] == board[2] && board[2] == board[3])
            {

                return "Win";


            }
            if (board[4] == board[5] && board[5] == board[6])
            {

                return "Win";

            }
            if (board[7] == board[8] && board[8] == board[9])
            {

                return "Win";

            }
            if (board[1] == board[4] && board[4] == board[7])
            {

                return "Win";

            }
            if (board[2] == board[5] && board[5] == board[8])
            {

                return "Win";

            }
            if (board[3] == board[6] && board[6] == board[9])
            {

                return "Win";

            }
            if (board[1] == board[5] && board[5] == board[9])
            {

                return "Win";

            }
            if (board[3] == board[5] && board[5] == board[7])
            {

                return "Win";

            }
            if (board[1] != "1" && board[2] != "2" && board[3] != "3" && board[4] != "4" && board[5] != "5" && board[6] != "6" && board[7] != "7" && board[8] != "8" && board[9] != "9")
            {
                return "Tie";
            }
            return "Not Win";

        }
        public static void prepareBoard()
        {
            Console.Clear();
            Console.WriteLine(player1 + " is X and " + player2 + " is O!\n");
            gameBoard();
        }
        public static void checkWhosTurn()
        {
            if (playerCounter % 2 == 0)
            {
                Console.WriteLine("\n" + player2 + "'s turn!\n");

            }
            else
            {
                Console.WriteLine("\n" + player1 + "'s turn!\n");
            }
        }
        public static void switchPlayers()
        {
            if (numOfTimesPlayed >= 1)
            {

                if (makeSureSwitchFirstTurn % 2 == 0)
                {
                    Console.WriteLine("\n" + player2 + " gets to go first now!");
                    Console.WriteLine("Press any key to continue");
                    playerCounter = 2;
                    makeSureSwitchFirstTurn++;
                    Console.ReadKey();

                }
                else
                {
                    Console.WriteLine("\n" + player1 + " gets to go first now!");
                    Console.WriteLine("Press any key to continue");
                    makeSureSwitchFirstTurn++;
                    Console.ReadKey();
                }


            }
        }
        public static void againstComputerOrPerson()
        {
            Console.WriteLine("How would you like to play?");
            Console.WriteLine("Type 1 or 2 to pick how you would like to play.");
            Console.WriteLine("Choice 1: Player vs. Player");
            Console.WriteLine("Choice 2: Player vs. Computer");
            do
            {
                gameTypeChoice = Console.ReadLine();
                if (gameTypeChoice == "1")
                {
                    Console.Clear();
                    Console.WriteLine("First player please enter your name");
                    player1 = Console.ReadLine();
                    Console.Clear();
                    Console.WriteLine("Second player please enter your name");
                    player2 = Console.ReadLine();
                    Console.Clear();
                    gameTypeChoiceCount = 1;
                }
                else if (gameTypeChoice == "2")
                {
                    player2 = "Computer";
                    Console.Clear();
                    Console.WriteLine("Human player! Give me your name!");
                    player1 = Console.ReadLine();
                    gameTypeChoiceCount = 1;
                    playWithComputer = 1;

                }
                else
                {
                    Console.WriteLine("You still need to select your what type of game you would like to play.\nEnter either 1 or 2.");
                    gameTypeChoiceCount = 0;
                }
            } while (gameTypeChoiceCount == 0);
        }
        public static void computerPlay()
        {

            #region CHECK FOR WIN OR BLOCK

            if (board[4] == board[5] && board[6] == "6")
            {
                choiceString = board[6];

                return;
            }
            if (board[5] == board[6] && board[4] == "4")
            {
                choiceString = board[4];

                return;
            }
            if (board[2] == board[5] && board[8] == "8")
            {
                choiceString = board[8];
                return;
            }
            if (board[5] == board[8] && board[2] == "2")
            {
                choiceString = board[2];
                return;
            }
            if (board[2] == board[8] && board[5] == "5")
            {
                choiceString = board[5];
                return;
            }
            
            if (board[3] == board[2] && board[1] == "1")
            {
                choiceString = board[1];
                return;
            }
            if (board[1] == board[2] && board[3] == "3")
            {
                choiceString = board[3];
                return;
            }
            if (board[1] == board[3] && board[2] == "2")
            {
                choiceString = board[2];
                return;
            }
            if (board[1] == board[7] && board[4] == "4")
            {
                choiceString = board[4];
                return;
            }
            if (board[1] == board[4] && board[7] == "7")
            {
                choiceString = board[7];
                return;
            }
            if (board[4] == board[7] && board[1] == "1")
            {
                choiceString = board[1];
                return;
            }
            if (board[7] == board[8] && board[9] == "9")
            {
                choiceString = board[9];
                return;
            }
            if (board[7] == board[9] && board[8] == "8")
            {
                choiceString = board[8];
                return;
            }
            if (board[9] == board[8] && board[7] == "7")
            {
                choiceString = board[7];
                return;
            }
            if (board[3] == board[6] && board[9] == "9")
            {
                choiceString = board[9];
                return;
            }
            if (board[3] == board[9] && board[6] == "6")
            {
                choiceString = board[6];
                return;
            }
            if (board[9] == board[6] && board[3] == "3")
            {
                choiceString = board[3];
                return;
            }
            if (board[7] == board[5] && board[3] == "3")
            {
                choiceString = board[3];
                return;
            }
            if (board[7] == board[3] && board[5] == "5")
            {
                choiceString = board[5];
                return;
            }
            if (board[3] == board[5] && board[7] == "7")
            {
                choiceString = board[7];
                return;
            }
            if (board[1] == board[9] && board[5] == "O")
            {
                choiceString = board[8];
                return;
            }
            if (board[3] == board[7] && board[5] == "O")
            {
                choiceString = board[8];
                return;
            }
            #endregion

            #region IF MIDDLE ISNT TAKEN, ALWAYS TAKE MIDDLE FIRST, IF MIDDLE IS TAKEN FIRST MOVE TAKE A CORNER

            if (board[5] == "5")
            {
                choiceString = board[5];
                return;
            }
            else
            {
                Random pickRandCorner = new Random();
                corner = pickRandCorner.Next(1, 4);

                if (corner == 1 && board[1] == "1")
                {
                    choiceString = board[1];
                    return;
                }
                else if (corner == 1 && board[1] != "1")
                {
                    corner++;
                }
                if (corner == 2 && board[3] == "3")
                {
                    choiceString = board[3];
                    return;
                }
                else if (corner == 2 && board[3] != "3")
                {
                    corner++;
                }
                if (corner == 3 && board[7] == "7")
                {
                    choiceString = board[7];
                    return;
                }
                else if (corner == 3 && board[7] != "7")
                {
                    corner++;
                }
                if (corner == 4 && board[9] == "9")
                {
                    choiceString = board[9];
                    return;
                }
                else if (corner == 4 && board[9] != "9")
                {
                    corner++;
                }
                #endregion

                #region IF ALL SPOTS FILLED BESIDES ONE, IT WILL BE A TIE SO FILL EMPTY SPOT SO GAME WILL END

                if (board[1] == "1")
                {
                    choiceString = "1";
                    return;
                }
                if (board[2] == "2")
                {
                    choiceString = "2";
                    return;
                }
                if (board[3] == "3")
                {
                    choiceString = "3";
                    return;
                }
                if (board[4] == "4")
                {
                    choiceString = "4";
                    return;
                }
                if (board[5] == "5")
                {
                    choiceString = "5";
                    return;
                }
                if (board[6] == "6")
                {
                    choiceString = "6";
                    return;
                }
                if (board[7] == "7")
                {
                    choiceString = "7";
                    return;
                }
                if (board[8] == "8")
                {
                    choiceString = "8";
                    return;
                }
                if (board[9] == "9")
                {
                    choiceString = "9";
                    return;
                }
                #endregion

            }
        }
        public static void createStatsFile()
        {
           
            string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);

            if (gameCounter == 1)
            {
                using (StreamWriter writer = new StreamWriter(desktopPath + @"\tictactoe_stats.txt", true))
                {
                    writer.WriteLine("\n\nGame {0}:",gameCounter);
                    writer.WriteLine("  " + board[1] + " | " + board[2] + " | " + board[3]);
                    writer.WriteLine(" -----------");
                    writer.WriteLine("  " + board[4] + " | " + board[5] + " | " + board[6]);
                    writer.WriteLine(" -----------");
                    writer.WriteLine("  " + board[7] + " | " + board[8] + " | " + board[9]);
                    
                    writer.WriteLine("Total games played: {0}", gameCounter);
                    writer.WriteLine("Total tied games: {0}", numOfTiedGame);
                    int player1Losses = gameCounter - numOfGamesWon1;
                    int player2Losses = gameCounter - numOfGamesWon2;
                    int player1percW = numOfGamesWon1 / gameCounter;
                    int player2percW = numOfGamesWon2 / gameCounter;
                    int player1percL = player1Losses / gameCounter;
                    int player2percL = player2Losses / gameCounter;
                    writer.WriteLine("{0} Total Won: {1}",player1, numOfGamesWon1);
                    writer.WriteLine("{0} Total Loss: {1}", player1, player1Losses);
                    writer.WriteLine("{0} Total Won: {1}", player2, numOfGamesWon2);
                    writer.WriteLine("{0} Total Loss: {1}", player2, player2Losses);
                    writer.WriteLine("{0} Total Win Percentage: {1}", player1, player1percW.ToString("P"));
                    writer.WriteLine("{0} Total Loss Percentage: {1}", player1, player1percL.ToString("P"));
                    writer.WriteLine("{0} Total Win Percentage: {1}", player2, player2percW.ToString("P"));
                    writer.WriteLine("{0} Total Loss Percentage: {1}", player2, player2percL.ToString("P"));
                }
            }

                if (gameCounter > oldCount)
                {
                    using (StreamWriter writer = new StreamWriter(desktopPath+@"\tictactoe_stats.txt", true))
                    {
                    writer.WriteLine("\n\nGame {0}:", gameCounter);
                    writer.WriteLine("  " + board[1] + " | " + board[2] + " | " + board[3]);
                    writer.WriteLine(" -----------");
                    writer.WriteLine("  " + board[4] + " | " + board[5] + " | " + board[6]);
                    writer.WriteLine(" -----------");
                    writer.WriteLine("  " + board[7] + " | " + board[8] + " | " + board[9]);

                    writer.WriteLine("Total games played: {0}", gameCounter);
                    writer.WriteLine("Total tied games: {0}", numOfTiedGame);
                    int player1Losses = gameCounter - numOfGamesWon1;
                    int player2Losses = gameCounter - numOfGamesWon2;
                    int player1percW = numOfGamesWon1 / gameCounter;
                    int player2percW = numOfGamesWon2 / gameCounter;
                    int player1percL = player1Losses / gameCounter;
                    int player2percL = player2Losses / gameCounter;
                    writer.WriteLine("{0} Total Won: {1}", player1, numOfGamesWon1);
                    writer.WriteLine("{0} Total Loss: {1}", player1, player1Losses);
                    writer.WriteLine("{0} Total Won: {1}", player2, numOfGamesWon2);
                    writer.WriteLine("{0} Total Loss: {1}", player2, player2Losses);
                    writer.WriteLine("{0} Total Win Percentage: {1}", player1, player1percW.ToString("P"));
                    writer.WriteLine("{0} Total Loss Percentage: {1}", player1, player1percL.ToString("P"));
                    writer.WriteLine("{0} Total Win Percentage: {1}", player2, player2percW.ToString("P"));
                    writer.WriteLine("{0} Total Loss Percentage: {1}", player2, player2percL.ToString("P"));
                }
                }
                oldCount = gameCounter;

            }
        }

    }



