﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TICTACTOE;

namespace TicTacToe.tests
{
    [TestFixture]
    class tests
    {
        [Test]
        public void TestBoard()
        {
            //arrange
            string choice = "X";
            string[] board = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
            board[1] = choice;
            board[2] = choice;
            board[3] = choice;
            string expected = "Win";

            //act
            string actual = "";
            if (board[1] == board[2] && board[2] == board[3])
            {

                actual = "Win";

            }
            //assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestDraw()
        {
            //arrange
            string expected = "Tie";
            string[] board = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
            board[1] = "X";
            board[2] = "X";
            board[3] = "X";
            board[4] = "X";
            board[5] = "X";
            board[6] = "X";
            board[7] = "X";
            board[8] = "X";
            board[9] = "X";

            //act
            string actual = "";
            if (board[1] != "1" && board[2] != "2" && board[3] != "3" && board[4] != "4" && board[5] != "5" && board[6] != "6" && board[7] != "7" && board[8] != "8" && board[9] != "9")
            {
                 actual = "Tie";
            }
            
            //assert
            Assert.AreEqual(expected, actual);
        }
    }

}
