Create Database MovieCatalogue
GO

USE MovieCatalogue
GO

Create Table Movie
(
MovieID int Identity (1,1) NOT NULL Primary Key,
Title varchar(30) NOT NULL ,
RunTime int NULL,
Rating varchar(5) NULL,
ReleaseDate Date NULL,
)