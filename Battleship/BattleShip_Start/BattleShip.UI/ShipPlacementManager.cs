﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;
using BattleShip.BLL.GameLogic;


namespace BattleShip.UI
{
    public class ShipPlacementManager
    {
        static string[,] Brd = new string[11, 11];
        public void ShipPlacementManage(Player player1, Player player2)
        {

            for (int x = 0; x < 10; x++)
            {
                for (int y = 0; y < 10; y++)
                {
                    Brd[x, y] = " ";
                }
                Console.WriteLine();
            }

            PlaceShips Player1PlaceShips = new PlaceShips();
            PlaceShips Player2PlaceShips = new PlaceShips();

            //PrintBlankBoard();
            //THIS LOOP CYCLES THROUGH UNTIL ALL SHIPS ARE PLACED FOR BOTH PLAYERS
            int counter = 0;
            while (player2.playerBoard._currentShipIndex < 5)
            {
                
                if (player1.playerBoard._currentShipIndex == 5 && counter == 0)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        for (int i = 0; i < 10; i++)
                        {
                            Brd[j, i] = " ";
                        }
                    }
                    counter = 1;
                }

                var CurrentBoardSetUp = player2.playerBoard;
                var CurrentPlayerPlaceShips = Player2PlaceShips;
                if (player1.playerBoard._currentShipIndex < 5)
                {
                    CurrentBoardSetUp = player1.playerBoard;
                    CurrentPlayerPlaceShips = Player1PlaceShips;
                }
                else if(player2.playerBoard._currentShipIndex == 0 && player1.playerBoard._currentShipIndex == 5)
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("It is player two's turn! Player one please step away! Press any key to continue");
                    Console.ResetColor();
                    Console.ReadKey();
                }
                
                Console.Clear();
                int x = 0;
                int y = 10;
                string blank = "";
                PlaceBoardPieces(x, y, blank);
                string userDirectionChoice = "";
                string userInputCoord = "";
                bool verifiedPlacement = true;
                do
                {
                    int xCordInt = 0;
                    int yCordInt = 0;
                    
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("Pick starting cord for where you want to place your ship (example: B9)");
                    userInputCoord = Console.ReadLine();
                    if (userInputCoord.Length <= 3 && userInputCoord.Length != 0)
                    {
                        //pull just the y-coord from the user's input and change to int
                        string yCoord = userInputCoord.Remove(0, 1);
                        char xCoord = userInputCoord[0];
                        xCordInt = Coordinate.TranslateXCoordinate(xCoord);
                        if (char.IsNumber(xCoord))
                        {
                            Console.WriteLine("Please enter a valid coordinate!");
                        }
                        else if (Int32.TryParse(yCoord, out yCordInt))
                        {
                            verifiedPlacement = false;
                            break;
                        }
                        else
                        {
                            Console.WriteLine("Please enter a valid coordinate!");
                        }
                    }
                    else if(userInputCoord.Length > 3 || userInputCoord.Length ==0)
                    {
                        Console.WriteLine("Please enter a valid coordinate!");
                    }
                } while (verifiedPlacement);

                do
                { 
                    Console.WriteLine("\nNow pick direction for ship to face");

                    Console.WriteLine("1: Up   2: Down   3: Left   4: Right");
                    userDirectionChoice = Console.ReadLine();
                    if (userDirectionChoice != "1" && userDirectionChoice != "2" && userDirectionChoice != "3" && userDirectionChoice != "4")
                    {
                        Console.WriteLine("You need to enter 1, 2, 3, or 4");
                    }

                } while (userDirectionChoice != "1" && userDirectionChoice != "2" && userDirectionChoice != "3" && userDirectionChoice != "4");
                Console.ResetColor();

                if (CurrentBoardSetUp._currentShipIndex == 0)
                {
                    ShipPlacement response = CurrentPlayerPlaceShips.PlaceCurrentShip(CurrentBoardSetUp, userInputCoord, userDirectionChoice);
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("Ship Placement Status: {0}         Press any key to continue", response);
                    Console.ResetColor();
                    PrintBoardForShipPlacement(userInputCoord, response, CurrentBoardSetUp._currentShipIndex, userDirectionChoice);

                }
                else if (CurrentBoardSetUp._currentShipIndex == 1)
                {

                    ShipPlacement response = CurrentPlayerPlaceShips.PlaceCurrentShip(CurrentBoardSetUp, userInputCoord, userDirectionChoice);
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("Ship Placement Status: {0}         Press any key to continue", response);
                    Console.ResetColor();
                    PrintBoardForShipPlacement(userInputCoord, response, CurrentBoardSetUp._currentShipIndex, userDirectionChoice);

                }
                else if (CurrentBoardSetUp._currentShipIndex == 2)
                {

                    ShipPlacement response = CurrentPlayerPlaceShips.PlaceCurrentShip(CurrentBoardSetUp, userInputCoord, userDirectionChoice);
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("Ship Placement Status: {0}         Press any key to continue", response);
                    Console.ResetColor();
                    PrintBoardForShipPlacement(userInputCoord, response, CurrentBoardSetUp._currentShipIndex, userDirectionChoice);

                }
                else if (CurrentBoardSetUp._currentShipIndex == 3)
                {

                    ShipPlacement response = CurrentPlayerPlaceShips.PlaceCurrentShip(CurrentBoardSetUp, userInputCoord, userDirectionChoice);
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("Ship Placement Status: {0}         Press any key to continue", response);
                    Console.ResetColor();
                    PrintBoardForShipPlacement(userInputCoord, response, CurrentBoardSetUp._currentShipIndex, userDirectionChoice);

                }
                else if (CurrentBoardSetUp._currentShipIndex == 4)
                {
                    ShipPlacement response = CurrentPlayerPlaceShips.PlaceCurrentShip(CurrentBoardSetUp, userInputCoord, userDirectionChoice);
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("Ship Placement Status: {0}         Press any key to continue", response);
                    Console.ResetColor();
                    PrintBoardForShipPlacement(userInputCoord, response, CurrentBoardSetUp._currentShipIndex, userDirectionChoice);

                }
                Console.ReadKey();

            }

        }

        public static void PlaceBoardPieces(int x, int y, string ship)
        {
            Brd[y, x] = ship;
            if (x==0 && y==10)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("\n    A   B   C   D   E   F   G   H   I   J ");
                Console.WriteLine("   ---------------------------------------");
                Console.WriteLine(" 1| {0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} |", Brd[0, 0], Brd[0, 1], Brd[0, 2], Brd[0, 3], Brd[0, 4], Brd[0, 5], Brd[0, 6], Brd[0, 7], Brd[0, 8], Brd[0, 9], Brd[0, 10]);
                Console.WriteLine(" 2| {0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} |", Brd[1, 0], Brd[1, 1], Brd[1, 2], Brd[1, 3], Brd[1, 4], Brd[1, 5], Brd[1, 6], Brd[1, 7], Brd[1, 8], Brd[1, 9]);
                Console.WriteLine(" 3| {0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} |", Brd[2, 0], Brd[2, 1], Brd[2, 2], Brd[2, 3], Brd[2, 4], Brd[2, 5], Brd[2, 6], Brd[2, 7], Brd[2, 8], Brd[2, 9]);
                Console.WriteLine(" 4| {0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} |", Brd[3, 0], Brd[3, 1], Brd[3, 2], Brd[3, 3], Brd[3, 4], Brd[3, 5], Brd[3, 6], Brd[3, 7], Brd[3, 8], Brd[3, 9]);
                Console.WriteLine(" 5| {0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} |", Brd[4, 0], Brd[4, 1], Brd[4, 2], Brd[4, 3], Brd[4, 4], Brd[4, 5], Brd[4, 6], Brd[4, 7], Brd[4, 8], Brd[4, 9]);
                Console.WriteLine(" 6| {0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} |", Brd[5, 0], Brd[5, 1], Brd[5, 2], Brd[5, 3], Brd[5, 4], Brd[5, 5], Brd[5, 6], Brd[5, 7], Brd[5, 8], Brd[5, 9]);
                Console.WriteLine(" 7| {0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} |", Brd[6, 0], Brd[6, 1], Brd[6, 2], Brd[6, 3], Brd[6, 4], Brd[6, 5], Brd[6, 6], Brd[6, 7], Brd[6, 8], Brd[6, 9]);
                Console.WriteLine(" 8| {0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} |", Brd[7, 0], Brd[7, 1], Brd[7, 2], Brd[7, 3], Brd[7, 4], Brd[7, 5], Brd[7, 6], Brd[7, 7], Brd[7, 8], Brd[7, 9]);
                Console.WriteLine(" 9| {0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} |", Brd[8, 0], Brd[8, 1], Brd[8, 2], Brd[8, 3], Brd[8, 4], Brd[8, 5], Brd[8, 6], Brd[8, 7], Brd[8, 8], Brd[8, 9]);
                Console.WriteLine("10| {0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} |", Brd[9, 0], Brd[9, 1], Brd[9, 2], Brd[9, 3], Brd[9, 4], Brd[9, 5], Brd[9, 6], Brd[9, 7], Brd[9, 8], Brd[9, 9]);
                Console.ResetColor();
            }
        }
        public static void PrintBoardForShipPlacement(string userInput, object response, int currentShip, object userDirectionChoice)
        {
            char xCoord = userInput[0];
            int xCordInt = Coordinate.TranslateXCoordinate(xCoord);
            //pull just the y-coord from the user's input and change to int
            string yCoord = userInput.Remove(0, 1);
            int yCordInt = Int32.Parse(yCoord);

            string responseConv = response.ToString();

            if (responseConv == "Ok")
            {
                string currentShipConv = currentShip.ToString();
                string userDirectionChoiceConv = userDirectionChoice.ToString();

                string ship = "";
                int shipLengthToPrint = 0;

                if (currentShip == 1)
                {
                    ship = "D";
                    shipLengthToPrint = 2;
                }
                if (currentShip == 2)
                {
                    ship = "C";
                    shipLengthToPrint = 3;
                }
                if (currentShip == 3)
                {
                    ship = "S";
                    shipLengthToPrint = 3;
                }
                if (currentShip == 4)
                {
                    ship = "B";
                    shipLengthToPrint = 4;
                }
                if (currentShip == 5)
                {
                    ship = "B";
                    shipLengthToPrint = 5;
                }


                xCordInt--;
                yCordInt--;
                ReturnIntAndShipFill(xCordInt, yCordInt, currentShip, userDirectionChoiceConv);

                PlaceBoardPieces(xCordInt, yCordInt, ship);

            }
        }

        public static void ReturnIntAndShipFill(int xCordInt, int yCordInt, int currentShip, string userDirectionChoiceConv)
        {
            string ship = "";
            int shipLengthToPrint = 0;

            if (currentShip == 1)
            {
                ship = "D";
                shipLengthToPrint = 2;
            }
            if (currentShip == 2)
            {
                ship = "C";
                shipLengthToPrint = 3;
            }
            if (currentShip == 3)
            {
                ship = "S";
                shipLengthToPrint = 3;
            }
            if (currentShip == 4)
            {
                ship = "B";
                shipLengthToPrint = 4;
            }
            if (currentShip == 5)
            {
                ship = "B";
                shipLengthToPrint = 5;
            }

            if (userDirectionChoiceConv == "1")
            {
                for (int i = 1; i <= shipLengthToPrint; i++)
                {
                    Brd[yCordInt, xCordInt] = ship;
                    yCordInt--;
                }
            }
            if (userDirectionChoiceConv == "2")
            {
                for (int i = 1; i <= shipLengthToPrint; i++)
                {
                    Brd[yCordInt, xCordInt] = ship;
                    yCordInt++;
                }
            }
            if (userDirectionChoiceConv == "3")
            {
                for (int i = 1; i <= shipLengthToPrint; i++)
                {
                    Brd[yCordInt, xCordInt] = ship;
                    xCordInt--;
                }
            }
            if (userDirectionChoiceConv == "4")
            {
                for (int i = 1; i <= shipLengthToPrint; i++)
                {
                    Brd[yCordInt, xCordInt] = ship;
                    xCordInt++;
                }
            }

        }
    }
}
