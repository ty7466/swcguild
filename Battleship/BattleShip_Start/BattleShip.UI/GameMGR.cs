﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;

namespace BattleShip.UI
{
    public class GameMGR
    {
        public Player player1 = new Player();
        public Player player2 = new Player();
        public Player current = new Player();
        public Player opponent = new Player();

        public void GameManager()
        {
            Turn turn = new Turn();
            player1.Number = 1;
            player2.Number = 2;
            bool winner = false;
            bool valid = true;
            current = null;

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Welcome to Battleship!");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Player 1 please enter your name...");
            player1.Name = Console.ReadLine();
            Console.WriteLine("Player 2 please enter your name...");
            player2.Name = Console.ReadLine();
            Console.ResetColor();
            Console.Clear();

            ShipPlacementManager PlaceShip = new ShipPlacementManager();
            PlaceShip.ShipPlacementManage(player1, player2);

            do
            {
                SwitchPlayers();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("{0}, please step away. {1} hit enter to play", opponent.Name, current.Name);
                Console.ResetColor();
                Console.ReadLine();

                do
                {
                    //sends currents's shot to PlayerTurn to fire on opponents's board
                    valid = true;
                    turn.DrawBoard(opponent.playerBoard);
                    string playerShotCoord = GetShotCoord();

                    //Go through a turn
                    FireShotResponse response = turn.TakeTurn(opponent.playerBoard, playerShotCoord);

                    if (response.ShotStatus == ShotStatus.Victory)
                    {
                        winner = true;
                    }

                    if (response.ShotStatus == ShotStatus.Invalid || response.ShotStatus == ShotStatus.Duplicate)
                    {
                        valid = false;
                    }

                    //write result of turn to console
                    turn.DrawBoard(opponent.playerBoard);

                    string responseMsg = turn.FireShotResponseMessage(response);
                    Console.WriteLine(responseMsg + "\n" + "Press enter to continue");
                    Console.ReadLine();

                } while (!valid);
            } while (!winner);
        }

        public void SwitchPlayers()
        {
            if (current == null || current.Number == 2)
            {
                current = player1;
                opponent = player2;
            }
            else
            {
                current = player2;
                opponent = player1;
            }
        }

        public string GetShotCoord()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("{0}, enter a shot coordinate. (For example, F5)", current.Name);
            Console.ResetColor();
            string playerShotCoord = Console.ReadLine();
            Console.ResetColor();
            if (playerShotCoord.Length < 2)
            {
                GetShotCoord();
            }
                return playerShotCoord;

        }
    }
}
