﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;


namespace BattleShip.UI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            GameMGR mgr = new GameMGR();
            mgr.GameManager();
            string playAgain = "";
            Console.WriteLine("Would you like to play again [Y/N]");
            do
            {
                playAgain = Console.ReadLine();
                if (playAgain == "Y" || playAgain == "y")
                {
                    GameMGR mgrAgain = new GameMGR();
                    mgrAgain.GameManager();
                }
                else if (playAgain == "N" ||playAgain == "n")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("You need to pick Y or N!");
                }
            } while (playAgain != "Y" || playAgain != "y");
        }
    }
}












