﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;
using NUnit.Framework;

namespace Battleship.Tests
{
    [TestFixture]
    class TranslateXCoordinateTests
    {
        [Test]
        public void TransXCoordTest()
        {
            //ARRANGE
            char input = 'A';
            int expected = 1;
            char input2 = 'B';
            int expected2 = 2;
            char input3 = 'c';
            char input4 = 'd';
            int expected3 = 3;
            int expected4 = 4;
            char input5 = 'J';
            int expected5 = 10;

            //ACT
            int actual = Coordinate.TranslateXCoordinate(input);
            int actual2 = Coordinate.TranslateXCoordinate(input2);
            int actual3 = Coordinate.TranslateXCoordinate(input3);
            int actual4 = Coordinate.TranslateXCoordinate(input4);
            int actual5 = Coordinate.TranslateXCoordinate(input5);


            //ASSERT
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(expected2, actual2);
            Assert.AreEqual(expected3, actual3);
            Assert.AreEqual(expected4, actual4);
            Assert.AreEqual(expected5, actual5);
        } 
    }
}
