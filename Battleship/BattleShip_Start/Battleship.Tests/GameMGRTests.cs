﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;
using BattleShip.UI;
using NUnit.Framework;

namespace Battleship.Tests
{
    [TestFixture]
    public class GameMGRTests
    {
        private Board SetupBoard()
        {
            Board board = new Board();

            PlaceDestroyer(board);
            PlaceCruiser(board);
            PlaceSubmarine(board);
            PlaceBattleship(board);
            PlaceCarrier(board);

            return board;
        }

        private void PlaceCarrier(Board board)
        {
            var request = new PlaceShipRequest()
            {
                Coordinate = new Coordinate(4, 4),
                Direction = ShipDirection.Right,
                ShipType = ShipType.Carrier
            };

            board.PlaceShip(request);
        }

        private void PlaceBattleship(Board board)
        {
            var request = new PlaceShipRequest()
            {
                Coordinate = new Coordinate(10, 6),
                Direction = ShipDirection.Down,
                ShipType = ShipType.Battleship
            };

            board.PlaceShip(request);
        }

        private void PlaceSubmarine(Board board)
        {
            var request = new PlaceShipRequest()
            {
                Coordinate = new Coordinate(3, 5),
                Direction = ShipDirection.Left,
                ShipType = ShipType.Submarine
            };

            board.PlaceShip(request);
        }

        private void PlaceCruiser(Board board)
        {
            var request = new PlaceShipRequest()
            {
                Coordinate = new Coordinate(3, 3),
                Direction = ShipDirection.Up,
                ShipType = ShipType.Cruiser
            };

            board.PlaceShip(request);
        }

        private void PlaceDestroyer(Board board)
        {
            var request = new PlaceShipRequest()
            {
                Coordinate = new Coordinate(1, 8),
                Direction = ShipDirection.Right,
                ShipType = ShipType.Destroyer
            };

            board.PlaceShip(request);
        }

        [Test]
        private void TakeTurnTest()
        {
            var board = SetupBoard();
            string input = "C3";
            Turn testTurn = new Turn();
            FireShotResponse testResponse = new FireShotResponse();
            testResponse.ShotStatus = ShotStatus.Hit;

            FireShotResponse output = testTurn.TakeTurn(board, input);

            Assert.AreEqual(testResponse.ShotStatus, output.ShotStatus);
        }


        [Test]
        private void UserInputToBoardCoordinateTest()
        {
            string input = "B9";
            object obj = null;
            Coordinate otherCoordinate = obj as Coordinate;
            otherCoordinate.XCoordinate = 2;
            otherCoordinate.YCoordinate = 9;

            Coordinate result = CoordinateCreater.UserInputToBoardCoordinate(input);
            int x = 2;
            Assert.AreEqual(otherCoordinate.XCoordinate, result.XCoordinate);
        }


        [Test]
        public void SwitchPlayersTest()
        {
            Player player1 = new Player();
            Player player2 = new Player();
            Player current = new Player();
            current.Number = 1;

            GameMGR.SwitchPlayers();

            Assert.AreEqual(current, player2);
        }
    }
}

