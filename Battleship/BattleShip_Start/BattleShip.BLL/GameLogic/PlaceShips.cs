﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;

namespace BattleShip.BLL.GameLogic
{
    public class PlaceShips
    {
        private int CounterThatPicksWhatShip = 1;
        //THIS CLASS ONLY DEALS WITH PLACING SHIPS ON BOARD
        public ShipDirection UserDirectionChoice(string DirectionChoice)
        {


            var choice = new ShipDirection();
            if (DirectionChoice == "1")
            {
                choice = ShipDirection.Up;
                return choice;
            }
            if (DirectionChoice == "2")
            {
                choice = ShipDirection.Down;
                return choice;
            }
            if (DirectionChoice == "3")
            {
                choice = ShipDirection.Left;
                return choice;
            }
            if (DirectionChoice == "4")
            {
                choice = ShipDirection.Right;
                return choice;
            }
            else
            {
                throw new ArgumentException();
            }
        }


        public ShipPlacement PlaceCurrentShip(Board CurrentBoardSetUp, string userInput, string directionChoice)
        {

            char xCoord = userInput[0];
            int xCordInt = Coordinate.TranslateXCoordinate(xCoord);
            //pull just the y-coord from the user's input and change to int
            string yCoord = userInput.Remove(0, 1);
            int yCordInt = int.Parse(yCoord);
            ShipDirection choice = UserDirectionChoice(directionChoice);

            string emptyStr = "";
            ShipType currentShip = CurrentShipBeingPlaced(emptyStr);
            PlaceShipRequest request = new PlaceShipRequest()
            {
                Coordinate = new Coordinate(xCordInt, yCordInt),
                Direction = choice,
                ShipType = currentShip
            };
            ShipPlacement response = CurrentBoardSetUp.PlaceShip(request);
            string checkResponse = response.ToString();
            ShipType checkedReponse = CurrentShipBeingPlaced(checkResponse);
            return response;
        }
        public ShipType CurrentShipBeingPlaced(string checkResponse)
        {

            var currentShipIttration = new ShipType();
            if (checkResponse == "Ok")
            {
                CounterThatPicksWhatShip++;
                return currentShipIttration;
            }
            else if (CounterThatPicksWhatShip == 1)

            {
                currentShipIttration = ShipType.Destroyer;
                return currentShipIttration;
            }
            else if (CounterThatPicksWhatShip == 2)
            {
                currentShipIttration = ShipType.Submarine;
                return currentShipIttration;
            }
            else if (CounterThatPicksWhatShip == 3)
            {
                currentShipIttration = ShipType.Cruiser;
                return currentShipIttration;
            }
            else if (CounterThatPicksWhatShip == 4)
            {
                currentShipIttration = ShipType.Battleship;
                return currentShipIttration;
            }
            else if (CounterThatPicksWhatShip == 5)
            {
                currentShipIttration = ShipType.Carrier;
                return currentShipIttration;
            }
            else
            {
                throw new ArgumentException();
            }
        }
    }
}




