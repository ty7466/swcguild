﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;

namespace BattleShip.BLL.GameLogic
{
    public class Turn
    {
        public FireShotResponse TakeTurn(Board PlayerBoard, string input)
        {
            //turn user input string to type Coordinate and checks validity
            Coordinate playerShot = CoordinateCreater.UserInputToBoardCoordinate(input);

            //FirsShot calls CheckShipsForHit adds coord to ShotHistory if miss or hit
            FireShotResponse response = PlayerBoard.FireShot(playerShot);

            return response;
        }


        //to write results of shot to console
        public string FireShotResponseMessage(FireShotResponse response)
        {
            
            string responseMessage = "";
            switch (response.ShotStatus)
            {
                case ShotStatus.Invalid:
                    responseMessage = "Invalid shot - please try again";
                    //needs to send them back to pick another shot////
                    break;
                case ShotStatus.Duplicate:
                    responseMessage = "Duplicate shot, please choose another";
                    //needs to send them back to pick another shot//////
                    break;
                case ShotStatus.Miss:
                    responseMessage = "You missed!";
                    break;
                case ShotStatus.Hit:
                    responseMessage = string.Format("You hit my {0}!", response.ShipImpacted);
                    break;
                case ShotStatus.HitAndSunk:
                    responseMessage = string.Format("You sank my {0}!", response.ShipImpacted);
                    break;
                case ShotStatus.Victory:
                    responseMessage = string.Format("You sank my { 0}! You win!", response.ShipImpacted);
                    break;
            }
            return responseMessage;           
        }

        public void DrawBoard(Board playerBoard)
        {
            string[,] PrintBoard = new string[10, 10];
            int counter = 0;
            if ( counter == 0)
            {
                for (int j = 0; j < 10; j++)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        PrintBoard[j, i] = " ";
                    }
                }
                counter = 1;
            }
            Console.Clear();
            Console.WriteLine("\n     A B C D E F G H I J ");
            Console.WriteLine("    --------------------");
            int yAxisNaming2 = 0;

            //adds hits and misses to PrintBoard array
            foreach (var key in playerBoard.ShotHistory.Keys)
            {
                if (playerBoard.ShotHistory[key] == ShotHistory.Hit)
                {
                    PrintBoard[key.XCoordinate - 1, key.YCoordinate - 1] = "H";
                }
                else
                {
                    PrintBoard[key.XCoordinate - 1, key.YCoordinate - 1] = "M";
                }
            }

            for (int x = 0; x < 10; x++)
            {
                yAxisNaming2++; //this is just for printing y axis numbers 1-10
                if (yAxisNaming2 == 10)
                {
                    Console.Write(yAxisNaming2.ToString() + " | ");
                }
                else
                {
                    Console.Write(" " + yAxisNaming2.ToString() + " | ");
                }

                for (int y = 0; y < 10; y++)
                {
                    //print in color
                    if (PrintBoard[y, x] == "H")
                    {
                        Console.ForegroundColor = ConsoleColor.Red;

                        Console.Write("{0} ", PrintBoard[y, x]);

                        Console.ResetColor();
                    }
                    else if (PrintBoard[y, x] == "M")
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;

                        Console.Write("{0} ", PrintBoard[y, x]);


                        Console.ResetColor();
                    }
                    else
                    {

                        Console.Write("{0} ", PrintBoard[y, x]);
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
   

    

    

