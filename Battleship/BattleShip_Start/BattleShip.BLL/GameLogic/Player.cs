﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.BLL.GameLogic
{
    public class Player
    {
        public string Name;
        public Board playerBoard = new Board();
        public int Number;
    }
}
