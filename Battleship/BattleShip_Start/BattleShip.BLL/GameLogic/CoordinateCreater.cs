﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;

namespace BattleShip.BLL.GameLogic
{
    public class CoordinateCreater
    {
        //turn user input string to a game-friendly Coordinate
        public static Coordinate UserInputToBoardCoordinate(string input)
        {
            //pull letter from user's input and change to int
            char xCoord = input[0];
            int xCordInt = Coordinate.TranslateXCoordinate(xCoord);

            //pull just the y-coord from the user's input and change to int
            string yCoord = input.Remove(0, 1);
            int yCordInt;
            if (!Int32.TryParse(yCoord, out yCordInt))
            {
                yCordInt = 22;
            }

            //send int to Coordinate to create a coordinate the game likes
            Coordinate playerShot = new Coordinate(xCordInt, yCordInt);

            return playerShot;
        }

        

        private static bool IsValidCoordinate(Coordinate coordinate)
        {
            return coordinate.XCoordinate >= 1 && coordinate.XCoordinate <= 10 &&
            coordinate.YCoordinate >= 1 && coordinate.YCoordinate <= 10;
        }
    }
}
