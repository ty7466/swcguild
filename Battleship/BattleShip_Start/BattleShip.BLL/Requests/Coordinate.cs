﻿using System;

namespace BattleShip.BLL.Requests
{
    public class Coordinate
    {
        public int XCoordinate { get; set; }
        public int YCoordinate { get; set; }

        public Coordinate(int x, int y)
        {
            XCoordinate = x;
            YCoordinate = y;
        }

        public override bool Equals(object obj)
        {
            Coordinate otherCoordinate = obj as Coordinate;

            if (otherCoordinate == null)
                return false;

            return otherCoordinate.XCoordinate == this.XCoordinate &&
                   otherCoordinate.YCoordinate == this.YCoordinate;
        }
		
		public override int GetHashCode() 
        { 
            string uniqueHash = this.XCoordinate.ToString() + this.YCoordinate.ToString() + "00"; 
            return (System.Convert.ToInt32(uniqueHash)); 
        } 


        //turn XCoord letter to int
        public static int TranslateXCoordinate(char input)
        {
            //convert string input to char
            char inputChar = System.Convert.ToChar(input);

            //represent char as int, int value of A is 65, so have to subtract for a or A=1
            int coord = char.ToUpper(inputChar)-64;
            return coord;
        }
    }
}
