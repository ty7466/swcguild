Use MovieCatalogue;
Go


INSERT INTO Movie (Title,RunTime, Rating, ReleaseDate)
Values('Caddyshack',98,'R','7-25-1980')
Select * From Movie

INSERT INTO Movie (Title, Rating)
Values('Ghostbusters','PG')
Select * From Movie

INSERT INTO Movie (Title,RunTime, Rating, ReleaseDate)
Values('Groundhog Day',NULL,'PG', '06/12/1980')
Select * From Movie

insert into Movie(Title,RunTime,Rating)
values('The Lion King', 89,'G'),
('Beauty and the Beast',84,'G'),
('Aladdin',90,'G')
Select * from Movie