use HotelReservation;
go

insert into Room(RoomTypeID,RoomNumber,HotelID)
values (1,110,1),(1,111,1),(2,123,1),
(2,250,2),(1,312,2),(2,76,2),
(3,7,3),(1,12,3),(2,123,3)

select *
from Room

select Room.*
From Room
	inner join Hotel 
	on Room.HotelID = Hotel.HotelID
Where Hotel.HotelID = 1


insert into RoomType(RoomTypeName, MaxOccupancy)
values ('King',2),('Queens',4),('Suite',6)

select *
from RoomType